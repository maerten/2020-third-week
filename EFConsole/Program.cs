﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            NorthwindEntities db = new NorthwindEntities();
            //db.Database.Log = Console.WriteLine;
            foreach (var x in db.Products
                .Select(x => new { x.ProductName, x.UnitPrice, x.Category.CategoryName })
                ) Console.WriteLine(x);

            foreach (var c in db.Categories.Include("Products")
                .Select(x => new { x.CategoryName, x.Products.Count })
                ) Console.WriteLine(c);

        }
    }
}
