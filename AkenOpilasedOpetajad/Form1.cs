﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AkenOpilasedOpetajad
{
    public partial class Form1 : Form
    {
        List<Opilane> õpilased = new List<Opilane>();
        List<Opetaja> õpetajad = new List<Opetaja>();        

        public Form1()
        {
            InitializeComponent();
        }

        string kes;

        private void button1_Click(object sender, EventArgs e)
        {
            kes = "Õpilased";
            õpilased =
            System.IO.File.ReadAllLines("..\\..\\Õpilased.txt")
                .Where(x => x.Trim().Length > 0) //elimineerib tühjad read
                .Select(x => x.Split(','))
                .Select(x => new Opilane { Isikukood = x[0].Trim(), Nimi = x[1].Trim(), Klass = x[2].Trim() })
                .ToList();

            this.dataGridView1.DataSource = õpilased;
            button3.Visible = true;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            kes = "Õpetajad";
            õpetajad =
            System.IO.File.ReadAllLines("..\\..\\Õpetajad.txt")
                .Where(x=>x.Trim().Length>0) //elimineerib tühjad read
                .Select(x=> x.Replace(", ",",")) //sama kui trim
                .Select(x => x.Split(','))//.Select(y => y.Trim()).ToArray())
                .Select(x => new Opetaja { Isikukood = x[0].Trim(), Nimi = x[1].Trim(), Aine=x[2].Trim(), Klass = (x.Length>3 ? x[3].Trim() : "") })
                .ToList();

            this.dataGridView1.DataSource = õpetajad;
            button3.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (kes == "Õpilased")
            {
                System.IO.File.WriteAllLines("..\\..\\Õpilased muudetud.txt", õpilased.Select(x => $"{x.Isikukood}, {x.Nimi}, {x.Klass}"));
            }
            if (kes == "Õpetajad")
            {
                System.IO.File.WriteAllLines("..\\..\\Õpetajad muudetud.txt", õpetajad.Select(x => $"{x.Isikukood}, {x.Nimi}, {x.Aine}, {x.Klass}"));
            }
        }
    }

    class Opilane
    {
        public string Isikukood { get; set; }
        public string Nimi { get; set; }
        public string Klass { get; set; }

    }

    class Opetaja
    {
        public string Isikukood { get; set; }
        public string Nimi { get; set; }
        public string Aine { get; set; }
        public string Klass { get; set; }
    }
}
