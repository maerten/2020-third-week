﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace JSONspordipaev
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Tulemus> protoRida = new List<Tulemus>();
            var proto = System.IO.File.ReadAllLines("..\\..\\spordipäeva protokoll.txt")
                //.Where(x => x.Trim().Length > 0)
                .Select(x => x.Split(','))
                .Select(x => new Tulemus { Nimi = x[0].Trim(), Distants = x[1].Trim(), Aeg = x[2].Trim()})
                .Select(x => protoRida.Add(x));

            //string txtToJson = JsonConvert.SerializeObject();
            System.IO.File.WriteAllText("..\\..\\Proto.json", txtToJson);
        }
    }

    public class Tulemus
    {
        private string Nimi { get; set; }
        private double Distants { get; set; }
        private double Aeg { get; set; }
    }
}
