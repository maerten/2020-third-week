﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ToodeteAken
{
    public partial class Tooted : Form
    {
        NorthwindEntities db = new NorthwindEntities();
        int categoryId = 0;
        public Tooted()
        {
            InitializeComponent();

            this.comboBox1.DataSource =
                   (new string[] { "Kõik tooted" })
                   .Union(
                        db.Categories
                        .Select(x => x.CategoryName)
                   //.ToList()
                   )
                   .ToList()
                   ;

        }

        private void Tooted_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = db.Products
                .Where(x => categoryId == 0 || x.CategoryID == categoryId)
                .Select(x => new { x.ProductID, x.ProductName, x.UnitPrice, x.UnitsInStock, x.Category.CategoryName })
                .ToList();

            dataGridView1.Columns["UnitPrice"].DefaultCellStyle.Format = "F2";
            dataGridView1.Columns["UnitPrice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.Columns["UnitsInStock"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string catName = comboBox1.SelectedItem.ToString();
            categoryId = db.Categories.Where(x => x.CategoryName == catName).SingleOrDefault()?.CategoryID ?? 0;
            Tooted_Load(sender, e);
        }

        private void Tooted_Closing(object sender, FormClosingEventArgs e)
        {
            DialogResult vastus= MessageBox.Show("Kas salvestame muudatused?","Toodete rakendus sulgub", MessageBoxButtons.YesNoCancel);
            //label1.Text = vastus.ToString();
            if (vastus == DialogResult.Yes) ;
        }

        private void DataGridView1_CellDoubleClick(object sender, )

    }
}
