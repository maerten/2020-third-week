﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpordiAken
{
    public partial class Form1 : Form
    {
        List<Tulemus> protokoll = new List<Tulemus>();
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            protokoll=
            System.IO.File.ReadAllLines("..\\..\\spordipäeva protokoll.txt")
                .Skip(1)
                .Select(x => x.Split(','))
                .Select(x => new Tulemus { Nimi = x[0], Distants = int.Parse(x[1]), Aeg= int.Parse(x[2]) })
                .ToList();

            this.dataGridView1.DataSource = protokoll;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            System.IO.File.WriteAllLines("..\\..\\muudetud protokoll.txt",
                (new string[] { "Nimi, Distants, Aeg" }).Union(
                    protokoll
                    .Select(x => $"{x.Nimi}, {x.Distants}, {x.Aeg}"))
                    );
        }

        private void button3_Click(object sender, EventArgs e)
        {
            protokoll.Add(new Tulemus { Nimi = textBox1.Text, Distants = int.Parse(textBox2.Text), Aeg = double.Parse(textBox3.Text) });
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = protokoll;
        }
    }

    class Tulemus
    {
        public string Nimi { get; set; }
        public double Distants { get; set; }
        public double Aeg { get; set; }

        //public double Kiirus => Distants / Aeg;
    }
}
