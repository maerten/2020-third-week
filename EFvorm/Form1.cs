﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EFvorm
{
    public partial class Form1 : Form
    {
        NorthwindEntities db = new NorthwindEntities();
        int kategooriaID = 0;

        public Form1()
        {
            InitializeComponent();

            comboBox1.DataSource =
                (new string[] { "Kõik tooted" })
                .Union(
                    db.Categories
                    .Select(x => x.CategoryName)
                )
                .ToList();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = db.Products
                .Where(x => kategooriaID == 0 || x.CategoryID == kategooriaID)
                .Select(x => new { x.ProductID, x.ProductName, x.UnitPrice })
                .ToList();

            //formattimine
            dataGridView1.Columns["UnitPrice"].DefaultCellStyle.Format = "F2";
            dataGridView1.Columns["UnitPrice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.Columns["ProductID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string kategooriaNimi = comboBox1.SelectedItem.ToString();
            kategooriaID = db.Categories.Where(x => x.CategoryName == kategooriaNimi).SingleOrDefault()?.CategoryID ?? 0;
            Form1_Load(sender, e);
        }
    }
}
