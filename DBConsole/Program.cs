﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;      // see rida annab mul lihtsamad objektinimed DB-ga toimetamiseks

namespace DBConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString =
                //"Data Source=valiitsql.database.windows.net;Initial Catalog=Northwind;Persist Security Info=True;User ID=student;Password=Pa$$w0rd";
                @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Data\NOrthwind.mdf;Integrated Security=True;Connect Timeout=30";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select productname, unitprice from products";
                using (SqlCommand comm = new SqlCommand(commandText, conn))
                {


                    var R = comm.ExecuteReader();
                    while (R.Read())
                    {
                        Console.WriteLine($"{R["productname"]}\t{R["unitprice"]}");
                    }
                } // siin visatakse command mälust välja
            } // siin visatakse connection mälust välja

        }
    }
}
